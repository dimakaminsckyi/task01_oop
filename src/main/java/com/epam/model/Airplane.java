package com.epam.model;

public class Airplane extends Aircraft {

    public Airplane(String model, int loadCapacity, int flightRange, int fuelConsumption) {
        super(model, loadCapacity, flightRange, fuelConsumption);
    }

    public AircraftType getType() {
        return AircraftType.AIRPLANE;
    }
}
