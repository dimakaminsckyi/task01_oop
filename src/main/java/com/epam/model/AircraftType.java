package com.epam.model;

public enum AircraftType {
    BLIMP,
    HELICOPTER,
    AIRPLANE
}
