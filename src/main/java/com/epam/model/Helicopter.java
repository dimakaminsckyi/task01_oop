package com.epam.model;

public class Helicopter extends Aircraft {

    public Helicopter(String model, int loadCapacity, int flightRange, int fuelConsumption) {
        super(model, loadCapacity, flightRange, fuelConsumption);
    }

    @Override
    public AircraftType getType() {
        return AircraftType.HELICOPTER;
    }
}
