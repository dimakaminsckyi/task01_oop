package com.epam.model;

public class Blimp extends Aircraft {

    public Blimp(String model, int loadCapacity, int flightRange, int fuelConsumption) {
        super(model, loadCapacity, flightRange, fuelConsumption);
    }

    public AircraftType getType() {
        return AircraftType.BLIMP;
    }
}
