package com.epam.model;

import java.io.Serializable;

public abstract class Aircraft implements Serializable {

    private String model;
    private int loadCapacity;
    private int flightRange;
    private int fuelConsumption;

    public Aircraft(String model, int loadCapacity, int flightRange, int fuelConsumption) {
        this.model = model;
        this.loadCapacity = loadCapacity;
        this.flightRange = flightRange;
        this.fuelConsumption = fuelConsumption;
    }

    public int getLoadCapacity() {
        return loadCapacity;
    }

    public int getFlightRange() {
        return flightRange;
    }

    public int getFuelConsumption() {
        return fuelConsumption;
    }

    public abstract AircraftType getType();

    @Override
    public String toString() {
        return "Aircraft{" +
                "model='" + model + '\'' +
                ", loadCapacity=" + loadCapacity +
                ", flightRange=" + flightRange +
                ", fuelConsumption=" + fuelConsumption +
                '}';
    }
}
