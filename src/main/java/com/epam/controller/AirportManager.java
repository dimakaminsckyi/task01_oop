package com.epam.controller;

import com.epam.model.Aircraft;
import com.epam.model.AircraftType;

import java.util.List;
import java.util.Scanner;

public interface AirportManager extends AirportCrud {

    int countLoadCapacity();

    int countAircraftUnit();

    List<Aircraft> sortByFlightRange();

    List<Aircraft> getAircraftszByFuelConsumption(int range);
}
