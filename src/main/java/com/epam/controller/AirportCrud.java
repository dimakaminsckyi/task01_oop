package com.epam.controller;

import com.epam.model.Aircraft;
import com.epam.model.AircraftType;

import java.util.List;
import java.util.Scanner;

public interface AirportCrud {

    List<Aircraft> getAircraftList();

    void createAircraft(AircraftType type, Scanner scan);

    void generateRecords();
}
