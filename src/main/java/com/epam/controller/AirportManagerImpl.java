package com.epam.controller;

import com.epam.model.*;

import java.util.*;
import java.util.stream.Collectors;

public class AirportManagerImpl implements AirportManager {

    private List<Aircraft> aircrafts = new ArrayList<>();

    @Override
    public int countLoadCapacity() {
        return aircrafts.stream().mapToInt(Aircraft::getLoadCapacity).sum();
    }

    @Override
    public int countAircraftUnit() {
        return aircrafts.size();
    }

    @Override
    public List<Aircraft> sortByFlightRange() {
        return aircrafts.stream().sorted(Comparator.comparing(Aircraft::getFlightRange))
                .collect(Collectors.toList());
    }

    @Override
    public List<Aircraft> getAircraftszByFuelConsumption(int range) {
        return aircrafts.stream().filter(o -> o.getFuelConsumption() >= range).collect(Collectors.toList());
    }

    @Override
    public List<Aircraft> getAircraftList() {
        return aircrafts;
    }

    @Override
    public void createAircraft(AircraftType type, Scanner scan) {
        System.out.println("Aircraft model :");
        String model = scan.next();
        System.out.println("Aircraft load capacity:");
        int loadCapacity = scan.nextInt();
        System.out.println("Aircraft flight range:");
        int flightRange = scan.nextInt();
        System.out.println("Aircraft fuel consumption:");
        int fuelConsumption = scan.nextInt();
        switch (type) {
            case HELICOPTER:
                aircrafts.add(new Helicopter(model, loadCapacity, flightRange, fuelConsumption));
                break;
            case BLIMP:
                aircrafts.add(new Blimp(model, loadCapacity, flightRange, fuelConsumption));
                break;
            case AIRPLANE:
                aircrafts.add(new Airplane(model, loadCapacity, flightRange, fuelConsumption));
                break;
        }
        scan.nextLine();
    }

    @Override
    public void generateRecords() {
        aircrafts.add(new Helicopter("X-3", 1500, 5000, 400));
        aircrafts.add(new Blimp("Goodyear", 2000, 20000, 1000));
        aircrafts.add(new Airplane("Boing", 20000, 20000, 5000));
        aircrafts.add(new Helicopter("Z-25", 2000, 7000, 800));
    }
}
