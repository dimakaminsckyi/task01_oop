package com.epam.view;

import com.epam.controller.AirportManager;
import com.epam.controller.AirportManagerImpl;
import com.epam.model.AircraftType;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Menu {

    private Scanner scanner;
    private AirportManager airportManager;

    public Menu() {
        scanner = new Scanner(System.in);
        airportManager = new AirportManagerImpl();
        airportManager.generateRecords();
    }

    public void runMenu() {
        boolean flag = true;
        MenuOptions menuOptions;
        AircraftType aircraftType;
        try {
            while (flag) {
                showMenuOptions();
                menuOptions = MenuOptions.valueOf(scanner.nextLine());
                switch (menuOptions) {
                    case a:
                        airportManager.getAircraftList().forEach(System.out::println);
                        break;
                    case b:
                        System.out.println(airportManager.countLoadCapacity());
                        break;
                    case c:
                        System.out.println(airportManager.countAircraftUnit());
                        break;
                    case d:
                        airportManager.sortByFlightRange()
                                .forEach(System.out::println);
                        break;
                    case f:
                        System.out.println("Enter range :");
                        airportManager.getAircraftszByFuelConsumption(scanner.nextInt())
                                .forEach(System.out::println);
                        scanner.nextLine();
                        break;
                    case g:
                        System.out.println("Enter aircraft type: ");
                        aircraftType = AircraftType.valueOf(scanner.nextLine().toUpperCase());
                        airportManager.createAircraft(aircraftType, scanner);
                        break;
                    case e:
                        scanner.close();
                        flag = false;
                }
            }
        } catch (IllegalArgumentException | InputMismatchException e) {
            System.out.println("please enter correct input");
        }
    }

    private void showMenuOptions() {
        System.out.println("\na - Show all aircrafts");
        System.out.println("b - Show load capacity of Airport");
        System.out.println("c - Number of aircrafts");
        System.out.println("d - Show list of sort aircrafts by flight range");
        System.out.println("f - Show aircraft in range of fuel consumption");
        System.out.println("g - Add aircraft");
        System.out.println("e - Exit\n");
    }
}
