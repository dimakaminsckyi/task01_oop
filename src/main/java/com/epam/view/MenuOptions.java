package com.epam.view;

public enum MenuOptions {

    a("Show all aircrafts"),
    b("Show load capacity of Airport"),
    c("Number of aircrafts"),
    d("Show sort aircrafts by flight range"),
    f("Show aircraft in range of fuel consuption"),
    g("Add aircraft"),
    e("Exit");

    private String option;

    MenuOptions(String option) {
        this.option = option;
    }
}
